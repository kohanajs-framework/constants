# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2021-10-19)

## [1.1.0] - 2021-09-07
### Added
- create CHANGELOG.md